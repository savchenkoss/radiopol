#!/usr/bin/env python

import argparse
import math
import random
from pathlib import Path
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
from astropy.time import Time


class WCS:
    def __init__(self, fits_file_name):
        hdu = fits.open(fits_file_name)
        header = hdu[0].header
        # Reference pixel
        self.crpix1 = header["CRPIX1"]
        self.crpix2 = header["CRPIX2"]
        # World coordinate of reference pixel
        self.crval1 = header["CRVAL1"]
        self.crval2 = header["CRVAL2"]
        # Pixel sizes
        self.cdelt1 = header["CDELT1"]
        self.cdelt2 = header["CDELT2"]
        self.scale = abs(self.cdelt1)

    def world_to_pix(self, ra, dec):
        # Convert RA and Declination to pixel coordinates
        x = (ra - self.crval1) / self.cdelt1 + self.crpix1
        y = (dec - self.crval2) / self.cdelt2 + self.crpix2
        return x, y


class StokesImages:
    def __init__(self, base_path):
        directory = base_path.parent
        name_base = base_path.name
        self.data_all = {}
        self.wcs_all = {}
        for kind in "iqu":
            name = list(Path(directory).glob(f"{name_base}*{kind}map*"))[0]
            print(f"Loading {kind}-map: {name}")
            self.data_all[kind] = fits.getdata(name)[0, 0, :]
            self.wcs_all[kind] = WCS(name)
        self.y_size, self.x_size = self.data_all["i"].shape
        self.x_center = self.x_size / 2
        self.y_center = self.y_size / 2

    def compute_rms(self, components, radius=1):
        """ Compute an error estimate """
        # Compute mean coordinates of all components
        x_mean = np.mean([c.x for c in components])
        y_mean = np.mean([c.y for c in components])
        # Decide which corner of the image to take as a source of a pure noise
        noise_window_size = min(self.x_size, self.y_size) / 5
        noise_window_size -= noise_window_size % 2
        if (x_mean > self.x_center) and (y_mean > self.y_center):
            # Most of the components are in the upper right corner of the image, so
            # take the lower left corner as a noise source
            x_noise_center = noise_window_size
            y_noise_center = noise_window_size
        elif (x_mean > self.x_center) and (y_mean < self.y_center):
            # Lower right corner -> upper left corner
            x_noise_center = noise_window_size
            y_noise_center = self.y_size - noise_window_size
        elif (x_mean < self.x_center) and (y_mean < self.y_center):
            # Lower left corner -> upper right corner
            x_noise_center = self.x_size - noise_window_size
            y_noise_center = self.y_size - noise_window_size
        else:
            # Upper left corner -> lower right coner
            x_noise_center = self.x_size - noise_window_size
            y_noise_center = noise_window_size

        plt.imshow(self.data_all["i"], origin="lower")
        for comp in components:
            plt.scatter(comp.x, comp.y, 2*max(2, comp.fwhm_pixels), ec="k", fc="none")
        # plt.plot(self.x_center, self.y_center, "rx")
        # plt.plot(x_mean, y_mean, "k+")
        plt.fill_between([x_noise_center-noise_window_size, x_noise_center+noise_window_size],
                         [y_noise_center-noise_window_size, y_noise_center-noise_window_size],
                         [y_noise_center+noise_window_size, y_noise_center+noise_window_size],
                         hatch="x", fc="none", linewidth=0.0)
        # Now get random measurements inside the noise square
        n_tries = 1000
        i_noise = np.empty(n_tries)
        q_noise = np.empty(n_tries)
        u_noise = np.empty(n_tries)
        for idx in range(n_tries):
            x = random.randint(int(x_noise_center-noise_window_size+radius),
                               int(x_noise_center+noise_window_size-radius))
            y = random.randint(int(y_noise_center-noise_window_size+radius),
                               int(y_noise_center+noise_window_size-radius))
            i_noise[idx] = np.sum(self.data_all["i"][y-radius:y+radius+1, x-radius:x+radius+1])
            q_noise[idx] = np.sum(self.data_all["q"][y-radius:y+radius+1, x-radius:x+radius+1])
            u_noise[idx] = np.sum(self.data_all["u"][y-radius:y+radius+1, x-radius:x+radius+1])
        i_std = np.std(i_noise)
        q_std = np.std(q_noise)
        u_std = np.std(u_noise)
        return {"i": i_std, "q": q_std, "u": u_std}

    def get_stokes_vector(self, x_center, y_center, radius=1):
        """ Function returns flux at the given coordinates with taking into account
        that they can have not integer values"""
        fluxes = {}
        for kind in "iqu":
            total_flux = 0
            for dx in range(-radius, radius+1):
                for dy in range(-radius, radius+1):
                    x = x_center+dx
                    y = y_center+dy

                    fx, ix = math.modf(x)
                    fy, iy = math.modf(y)
                    ix = int(ix)
                    iy = int(iy)
                    total_flux += ((1.0-fx)*(1.0-fy)*self.data_all[kind][iy, ix] +
                                   fx*(1.0-fy)*self.data_all[kind][iy+1, ix] +
                                   fy*(1.0-fx)*self.data_all[kind][iy, ix+1] +
                                   fx*fy*self.data_all[kind][iy+1, ix+1])
            fluxes[kind] = total_flux
        return fluxes


class Component:
    def __init__(self, flux, ra, dec, fwhm, wcs):
        """
        RA and DEC in degrees, fwhm in arcseconds
        """
        self.flux = flux
        self.ra = ra
        self.dec = dec
        self.fwhm_arcseconds = fwhm
        self.x, self.y = wcs.world_to_pix(ra, dec)
        self.fwhm_pixels = (self.fwhm_arcseconds / 3600) / wcs.scale


def load_components(model_file, wcs):
    components = []
    flux, ra, dec, fwhm = np.genfromtxt(model_file, usecols=(0, 7, 8, 9), unpack=True)
    for i in range(len(ra)):
        components.append(Component(flux[i], ra[i], dec[i], fwhm[i], wcs))
    return components


def stokes_vector_to_polar_params(stokes_vector, stokes_vector_std):
    i = stokes_vector["i"]
    q = stokes_vector["q"]
    u = stokes_vector["u"]
    i_std = stokes_vector_std["i"]
    q_std = stokes_vector_std["q"]
    u_std = stokes_vector_std["u"]
    # Compute PD
    pd = (q**2 + u**2)**0.5 / i
    # Compute EVPA
    if q != 0.0:
        evpa = math.degrees(0.5 * math.atan2(u, q))
        if evpa < 0:
            evpa += 180
    else:
        if stokes_vector["u"] > 0.0:
            evpa = 45.0
        elif stokes_vector["u"] < 0.0:
            evpa = 135.0
        else:
            evpa = None

    # Compute errors
    # Compute PD error
    pd_sigma = (i_std**2 / i**4 + (q**2*q_std**2 + u**2 * u_std**2)/(i**2 * (q**2 + u**2))) ** 0.5
    # Compute EVPA error
    evpa_sigma = math.degrees((u**2*q_std**2 + q**2*u_std**2)**0.5 / (2*(q**2 + u**2)))
    return pd, evpa, pd_sigma, evpa_sigma


def main(args):
    obs_data = args.model.name.split("_")[0]
    year = int(obs_data[:4])
    month = int(obs_data[4:6])
    day = int(obs_data[6:])
    mjd = Time({"year": year, "month": month, "day": day}).mjd
    data = StokesImages(args.images)
    components = load_components(args.model, data.wcs_all["i"])
    stokes_vector_std = data.compute_rms(components, radius=args.radius)
    print(f"MJD: {mjd}")
    for idx, comp in enumerate(components):
        stokes_vector = data.get_stokes_vector(comp.x, comp.y, radius=args.radius)
        pd, evpa, pd_sigma, evpa_sigma = stokes_vector_to_polar_params(stokes_vector, stokes_vector_std)
        if pd > pd_sigma:
            print(f"Component {idx+1}: Flux={comp.flux:1.4e} PD={pd:1.3f} +/- {pd_sigma:1.3f};  EVPA={evpa:1.1f} +/- {evpa_sigma:1.1f}")
            # Plot polarization on the map
            length = pd * 1000
            x_start = comp.x - length*np.cos(np.radians(90+evpa))
            x_end = comp.x + length*np.cos(np.radians(90+evpa))
            y_start = comp.y - length*np.sin(np.radians(90+evpa))
            y_end = comp.y + length*np.sin(np.radians(90+evpa))
            plt.plot([x_start, x_end], [y_start, y_end], color="k")
        else:
            print(f"Component {idx+1}: Flux={comp.flux:1.4e}  PD=-----;  EVPA=-----")
    plt.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--images", type=Path)
    parser.add_argument("--model", type=Path)
    parser.add_argument("--radius", type=int, default=1)
    args = parser.parse_args()
    main(args)
