#!/usr/bin/env python

import math
import argparse
import numpy as np
import matplotlib.pyplot as plt
from astropy.time import Time


colors = {"i": "g", "q": "b", "u": "r"}


class StokesComponent:
    def __init__(self, flux, flux_std, ra, dec, size):
        self.flux = flux
        self.flux_std = flux_std
        self.ra = ra
        self.dec = dec
        self.size = size


class PolarPicker:
    def __init__(self, epoch):
        # Load all data
        self.i_flux, self.i_flux_std, self.i_ra, self.i_dec, self.i_size =\
            np.genfromtxt(f"{epoch}_i.smod", usecols=(0, 1, 7, 8, 9), unpack=True)
        self.q_flux, self.q_flux_std, self.q_ra, self.q_dec, self.q_size =\
            np.genfromtxt(f"{epoch}_q.smod", usecols=(0, 1, 7, 8, 9), unpack=True)
        self.u_flux, self.u_flux_std, self.u_ra, self.u_dec, self.u_size =\
            np.genfromtxt(f"{epoch}_u.smod", usecols=(0, 1, 7, 8, 9), unpack=True)
        # Parse date
        year = int(epoch[:4])
        month = int(epoch[4:6])
        day = int(epoch[6:])
        print(f"Loading epoch: {year}.{month:02}.{day:02}")
        self.mjd = Time({"year": year, "month": month, "day": day}).mjd
        # Make sure all arrays are arrays, but not floats
        self.i_flux = np.atleast_1d(self.i_flux)
        self.i_flux_std = np.atleast_1d(self.i_flux_std)
        self.i_ra = np.atleast_1d(self.i_ra)
        self.i_dec = np.atleast_1d(self.i_dec)
        self.i_size = np.atleast_1d(self.i_size)
        self.q_flux = np.atleast_1d(self.q_flux)
        self.q_flux_std = np.atleast_1d(self.q_flux_std)
        self.q_ra = np.atleast_1d(self.q_ra)
        self.q_dec = np.atleast_1d(self.q_dec)
        self.q_size = np.atleast_1d(self.q_size)
        self.u_flux = np.atleast_1d(self.u_flux)
        self.u_flux_std = np.atleast_1d(self.u_flux_std)
        self.u_ra = np.atleast_1d(self.u_ra)
        self.u_dec = np.atleast_1d(self.u_dec)
        self.u_size = np.atleast_1d(self.u_size)
        # Compute ranges of coordinates for pretty plotting
        min_ra = min([min(self.i_ra), min(self.q_ra), min(self.u_ra)])
        max_ra = max([max(self.i_ra), max(self.q_ra), max(self.u_ra)])
        ra_range = max_ra - min_ra
        min_dec = min([min(self.i_dec), min(self.q_dec), min(self.u_dec)])
        max_dec = max([max(self.i_dec), max(self.q_dec), max(self.u_dec)])
        dec_range = max_dec - min_dec
        min_ra -= 0.25 * ra_range
        max_ra += 0.25 * ra_range
        min_dec -= 0.25 * dec_range
        max_dec += 0.25 * dec_range
        ra_center = 0.5 * (min_ra + max_ra)
        dec_center = 0.5 * (min_dec + max_dec)
        # Prepare the plot
        self.fig, self.ax = plt.subplots()
        self.ax.set_xlim(min_ra, max_ra)
        self.ax.set_ylim(min_dec, max_dec)
        self.ax.plot(self.i_ra, self.i_dec, f"{colors['i']}o", label="i")
        self.ax.plot(self.q_ra, self.q_dec, f"{colors['q']}o", label="q")
        self.ax.plot(self.u_ra, self.u_dec, f"{colors['u']}o", label="u")
        self.ax.legend()
        self.ax.set_xlabel("RA [$\mu$]")
        self.ax.set_ylabel("DEC")
        x_ticks_locs = np.linspace(min_ra, max_ra, 5)
        x_ticks_labels = [f"{3600000*(c-ra_center):1.2f}" for c in x_ticks_locs]
        self.ax.set_xticks(x_ticks_locs, x_ticks_labels)
        x_ticks_locs = np.linspace(min_dec, max_dec, 5)
        x_ticks_labels = [f"{3600000*(c-dec_center):1.2f}" for c in x_ticks_locs]
        self.ax.set_yticks(x_ticks_locs, x_ticks_labels)
        self.ax.invert_xaxis()
        self.fig.canvas.draw()
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)
        self.fig.canvas.mpl_connect('key_press_event', self.on_press)
        self.selected_points = {'i': None, 'q': None, 'u': None}
        self.selected_plot_instances = []

    def on_click(self, event):
        # Handle mouse click events
        if event.xdata is None:
            return
        i_dists = np.hypot(self.i_ra-event.xdata, self.i_dec-event.ydata)
        q_dists = np.hypot(self.q_ra-event.xdata, self.q_dec-event.ydata)
        u_dists = np.hypot(self.u_ra-event.xdata, self.u_dec-event.ydata)
        selected_stokes = "iqu"[np.argmin([np.min(i_dists), np.min(q_dists), np.min(u_dists)])]
        match selected_stokes:
            case "i":
                idx = np.argmin(i_dists)
                if self.selected_points["i"] is None:
                    self.selected_points["i"] = StokesComponent(self.i_flux[idx], self.i_flux_std[idx],
                                                                self.i_ra[idx], self.i_dec[idx], self.i_size[idx])
                else:
                    self.selected_points["i"] = None
            case "q":
                idx = np.argmin(q_dists)
                if self.selected_points["q"] is None:
                    self.selected_points["q"] = StokesComponent(self.q_flux[idx], self.q_flux_std[idx],
                                                                self.q_ra[idx], self.q_dec[idx], self.q_size[idx])
                else:
                    self.selected_points["q"] = None
            case "u":
                idx = np.argmin(u_dists)
                if self.selected_points["u"] is None:
                    self.selected_points["u"] = StokesComponent(self.u_flux[idx], self.u_flux_std[idx],
                                                                self.u_ra[idx], self.u_dec[idx], self.u_size[idx])
                else:
                    self.selected_points["u"] = None
        self.plot_selection()

    def on_press(self, event):
        if event.key == " ":
            self.compute_polarization()

    def plot_selection(self):
        while self.selected_plot_instances:
            self.selected_plot_instances.pop().remove()
        for s, point in self.selected_points.items():
            if point is not None:
                p = self.ax.plot(point.ra, point.dec, f"{colors[s]}+", markersize=20)
                self.selected_plot_instances.append(p[0])
        self.fig.canvas.draw()

    def compute_polarization(self):
        if self.selected_points["i"] is None:
            print("Can not compute without I component")
            return
        if self.selected_points["q"] is None and self.selected_points["u"] is None:
            print("No polarization components selected")
            return
        if self.selected_points["q"] is not None:
            q = self.selected_points["q"].flux
            q_std = self.selected_points["q"].flux_std
        else:
            q = 0.0
            q_std = 0.0
        if self.selected_points["u"] is not None:
            u = self.selected_points["u"].flux
            u_std = self.selected_points["u"].flux_std
        else:
            u = 0.0
            u_std = 0.0
        i = self.selected_points["i"].flux
        i_std = self.selected_points["i"].flux_std
        # Compute PD
        pd = (q**2 + u**2)**0.5 / i
        # Compute EVPA
        if q != 0.0:
            evpa = math.degrees(0.5 * math.atan2(u, q))
            if evpa < 0:
                evpa += 180
        else:
            if u > 0.0:
                evpa = 45.0
            elif u < 0.0:
                evpa = 135.0
            else:
                evpa = None
        pd_sigma = (i_std**2 / i**4 + (q**2*q_std**2 + u**2 * u_std**2)/(i**2 * (q**2 + u**2))) ** 0.5
        # Compute PD error
        # Compute EVPA error
        evpa_sigma = math.degrees((u**2*q_std**2 + q**2*u_std**2)**0.5 / (2*(q**2 + u**2)))
        print(f"PD = {pd:1.3f} +/- {pd_sigma:1.3f}, EVPA={evpa:1.2f} +/- {evpa_sigma:1.3f}")
        print(f"{self.mjd}   {pd:1.3f}   {pd_sigma:1.3f}    {evpa:1.2f}   {evpa_sigma:1.3f}  {i}")


def main(args):
    p = PolarPicker(args.epoch)
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("epoch")
    args = parser.parse_args()
    main(args)
